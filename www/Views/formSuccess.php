<h2>Результат проведенных вычислений</h2>

<p><b>Исходная строка:</b> <?=$checkString?>

<p>
	<b>Полученные варианты подстрок длиной минимум <?=$minPartLength?>:</b>
	<br />
	<? if (empty($partitions)) { ?>

		Нет найденных значений
	<? } else { ?>

		<ul>
			<? foreach ($partitions as $part) { ?>
				<li><?=$part?></li>
			<? } ?>
		</ul>
	<? }?>
</p>