<?php

namespace StringPossiblePartitions;

/**
 * Получение списка из всех возможных разбиений строки на подстроки, с минимальной длиной подстроки
 *
 * Class StringPossiblePartitions
 */
class StringPossiblePartitions {

	/**
	 * Режим отладки
	 * @var bool
	 */
	private $debug;

	/**
	 * Поля формы
	 * @var array
	 */
	private static $formFields = [
		"checkString"   => ["name" => "Проверяемая строка", "type" => "text"],
		"minPartLength" => ["name" => "Минимальная длина подстроки", "type" => "number"]
	];

	/**
	 * Типы сообщений
	 * @var array
	 */
	private static $messageTypes = ['notice', 'success', 'error'];

	function __construct($debug = false) {

		if ($debug)
			$this->debug = $debug;

		if ($this->debug) {

			error_reporting(E_ALL);
			ini_set("display_errors", "on");
		}
	}

	/**
	 * Заголовок класса
	 *
	 * @return string
	 */
	protected function getTitle() {

		return "Получение списка из всех возможных разбиений строки на подстроки, с минимальной длиной подстроки";
	}

	/**
	 * Валидация пользовательской формы
	 *
	 * @param array $buffer
	 *
	 * @return bool
	 */
	private function formValidate(&$buffer) {

		$valid = true;

		foreach (self::$formFields as $key => $column) {
			if (empty($_POST[$key])) {

				$valid = false;
				$buffer[] = $this->show_message(sprintf('Не указано обязательное поле "%s"', $column["name"]), 'error');
			}
		}

		return $valid;
	}

	/**
	 * Отображение сервисного сообщения
	 *
	 * @param string $message
	 * @param string $type
	 *
	 * @return string
	 */
	protected function show_message($message, $type) {

		if (!in_array(strtolower($type), self::$messageTypes)) {

			return '';
		}

		return $this->getView('message' . ucfirst(strtolower($type)), ['message' => $message]);
	}

	/**
	 * Получение шаблона
	 *
	 * @param string      $name
	 * @param array       $data
	 * @param null|string $dir
	 *
	 * @return string
	 */
	protected function getView($name, array $data = [], $dir = null) {

		if (!$dir)
			$dir = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Views');

		ob_start();
		extract($data);
		@include($dir . DIRECTORY_SEPARATOR . $name . ".php");

		return ob_get_clean();
	}

	/**
	 * Создание пользовательской формы
	 *
	 * @return string
	 */
	private function createForm() {

		return $this->getView("requestForm", ['fields' => self::$formFields]);
	}

	/**
	 * Рекурсивный метод получения возможных слагаемых числа
	 *
	 * @param int   $allLength
	 * @param int   $minLength
	 * @param int   $maxLength
	 * @param array $result
	 * @param array $a
	 * @param int   $pos
	 */
	private function getPartition($allLength, $minLength, $maxLength, &$result, &$terms = [], $pos = 0) {

		if ($allLength > 0) {

			for ($i = $minLength; $i <= $maxLength; $i++) {

				$terms[$pos] = $i;

				$this->getPartition($allLength - $i, $minLength, min($i, $allLength - $i), $result, $terms, $pos + 1);
			}
		} else {

			if (!in_array($terms, $result)) {
				$result[] = $terms;
			}

			for ($i = 0; $i < count($terms) - 1; $i++) {

				array_push($terms, array_shift($terms));

				if (!in_array($terms, $result)) {
					$result[] = $terms;
				}
			}

			$terms = [];
		}
	}

	/**
	 * Получение возможных вариаций длин подстрок
	 *
	 * @param int $allLength
	 * @param int $minLength
	 *
	 * @return array
	 */
	private function getPartitionsLength($allLength, $minLength) {

		$partititons = [];

		$this->getPartition($allLength, $minLength, $allLength - $minLength, $partititons);

		return $partititons;
	}

	/**
	 * Получение подстрок по массиву их длин
	 *
	 * @param string $checkString
	 * @param array  $partitionsLength
	 *
	 * @return array
	 */
	private function getPartitionsSubstr($checkString, $partitionsLength) {

		$partitionsSubstr = [];

		foreach ($partitionsLength as $partitions) {

			$substr = [];
			$tmpString = $checkString;

			array_walk($partitions, function ($part) use (&$substr, &$tmpString) {

				$substr[] = substr($tmpString, 0, $part);
				$tmpString = substr($tmpString, $part);
			});

			$partitionsSubstr[] = implode(", ", $substr);

		}

		return $partitionsSubstr;
	}

	/**
	 * Обработка успешной формы
	 *
	 * @return string
	 */
	private function formSuccess() {

		$checkString = (string)$_POST["checkString"];
		$minPartLength = (int)$_POST["minPartLength"];

		//найти возможные комбинации длин подстрок на длине исходной
		$partitionsLength = $this->getPartitionsLength(strlen($checkString), $minPartLength);

		//по найденныйм срезам вернуть подстроки исходной
		$partitionsSubstr = $this->getPartitionsSubstr($checkString, $partitionsLength);

		return $this->getView("formSuccess", [
			'checkString'   => $checkString,
			'minPartLength' => $minPartLength,
			'partitions'    => $partitionsSubstr
		]);
	}

	/**
	 * Подготовка отображения страницы
	 *
	 * @return string
	 */
	public function renderPage() {

		$buffer [] = sprintf('<h1>%s</h1>', $this->getTitle());

		if (isset($_POST['submit']) && $this->formValidate($buffer)) {

			$buffer[] = $this->formSuccess();
		} else {

			$buffer[] = $this->createForm();
		}

		return $this->getView('mainTemplate', ['title' => $this->getTitle(), 'buffer' => implode('', $buffer)]);
	}

}